from django.apps import AppConfig


class CalcRestConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "calc_rest"
