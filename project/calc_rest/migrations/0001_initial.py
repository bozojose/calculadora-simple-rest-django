# Generated by Django 4.1.7 on 2023-03-20 20:35

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="Contenido",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("operacion", models.CharField(max_length=64)),
                ("numero1", models.IntegerField()),
            ],
        ),
    ]
