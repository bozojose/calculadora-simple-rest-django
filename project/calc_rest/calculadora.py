#!/usr/bin/python3

import sys
import operator
from django.http import HttpResponse

def calculadora(funcion, operando1, operando2):
    operaciones = {"suma":operator.add, "resta":operator.sub, "multi":operator.mul, "div":operator.truediv}
    try:
        if funcion in operaciones:
            operando1 = float(operando1)
            operando2 = float(operando2)
            return HttpResponse(operaciones[funcion](operando1,operando2))
        else:
            return HttpResponse('<h2>Operaciones disponibles: suma, resta, multi, div.</h2>')
    except ZeroDivisionError:
        return HttpResponse('<h2>División entre 0 no soportada. Por favor, introduzca un valor distinto de 0 para el operando2.</h2>')
    except ValueError:
        return HttpResponse("<h2>Por favor, introduzca como operadores valores numéricos.</h2>")


