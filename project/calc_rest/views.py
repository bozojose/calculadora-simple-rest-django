from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.views.decorators.csrf import csrf_exempt

from .calculadora import calculadora

from .models import Contenido

# Create your views here.

def index(request):
    return HttpResponse("<h2>Bienvenido/a. Estás en la página de inicio de la app calculadora simple REST.</h2>")

@csrf_exempt  # Decorador para pasar del csrf a partir de aquí
def calc(request, operacion):
    if request.method == "PUT":
        try:
            valor = request.body.decode('utf-8')  # Guarda el valor del cuerpo del PUT
            numero1 = valor.split(",")[0]
            numero2 = valor.split(",")[1]
            cuerpo = Contenido(operacion=operacion, numero1=numero1, numero2=numero2)
            cuerpo.save()
            mensaje = "<h2>Operación {operacion} realizada para los números {numero1} y {numero2}.</h2>"
            return HttpResponse(mensaje.format(operacion=operacion, numero1=numero1, numero2=numero2))
        except ValueError:
            return HttpResponse("<h2>Por favor, introduzca como operadores valores numéricos.</h2>")

    if request.method == "GET":
        content_list = Contenido.objects.all()
        id = len(content_list)
        try:
            contenido = get_object_or_404(Contenido, id=id, operacion=operacion)
            calculo = calculadora(operacion, contenido.numero1, contenido.numero2)
        except:
            calculo = '<h2>No se ha actualizado con la operación ' + operacion + ' para mostrar un nuevo resutado.</h2>'
        return HttpResponse(calculo)
